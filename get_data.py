import re
import requests
from pathlib import Path
import pandas as pd

NAMES = [
	'country/china',
	'country/italy',
	'country/south-korea',
	'country/iran',
	'country/france',
	'country/spain',
	'country/us',
	'country/germany',
	'country/uk',
	'coronavirus-cases',
]

POPULATION = {
	'country/china': 1_400_000_000,
	'country/italy': 60_000_000,
	'country/south-korea': 52_000_000,
	'country/iran': 83_000_000,
	'country/france': 67_000_000,
	'country/spain': 47_000_000,
	'country/us': 330_000_000,
	'country/germany': 83_000_000,
	'country/uk': 66_000_000,
	'residual': 5_500_000_000,
}

DATA = Path('data')

def get_cases(date, name):
	pickle_file = DATA / '{}/{}.pickle'.format(date.isoformat(), name)
	if pickle_file.exists():
		return pd.read_pickle(pickle_file)
	
	pickle_file.parent.mkdir(exist_ok = True, parents = True)
	
	r = requests.get('https://worldometers.info/coronavirus/{}'.format(name))
	r.raise_for_status()
	m = re.search(r'coronavirus-cases-linear(?:.+?)categories: \[([^]]*)\](?:.+?)data: \[([^]]*)\]', r.text)
	assert m is not None
	x = [d + ', 2020' for d in m.group(1).replace('"', '').split(',')]
	y = list(map(int, m.group(2).split(',')))
	df = pd.DataFrame({
		'date': x,
		'cases': y,
	})
	df['date'] = pd.to_datetime(df['date'])
	df.to_pickle(pickle_file)
	df.to_csv(pickle_file.with_suffix('.csv'), index = False)
	return df
