from datetime import datetime
import numpy as np
from scipy.optimize import minimize
from scipy.special import expit, gammaln
from matplotlib import pyplot as plt
import pandas as pd

from get_data import NAMES, get_cases, POPULATION

def main():
	today = datetime.utcnow().date()
	
	t0 = pd.Timestamp(2020, 1, 22)
	
	data = get_adjusted_data(today)
	
	models = []
	model_names = []
	
	for name, df in data.items():
		t = (df['date'] - t0).dt.days.values
		
		if name == 'coronavirus-cases':
			n_pred = multi_sigmoid_model(models, t)
			t1 = pd.Timestamp(2020, 2, 19)
			df_after = pd.DataFrame({
				'date': df['date'],
				'cases': df['cases'] - n_pred,
			})
			df_after = df_after[df_after['date'] >= t1]
			x = fit_sigmoid(t0, df_after, POPULATION['residual'])
			models.append(x)
			model_names.append('residual')
			
			n_pred = multi_sigmoid_model(models, t)
		else:
			x = fit_sigmoid(t0, df, POPULATION[name])
			models.append(x)
			model_names.append(name)
			n_pred = sigmoid_model(x, t)
		
		if name != 'coronavirus-cases':
			continue
		
		plt.title(name)
		legend = []
		
		if name == 'coronavirus-cases':
			future_days = pd.Series(np.arange(-45, 45))
			future_dates = pd.Timestamp(today) + pd.to_timedelta(future_days, unit = 'day')
			future_t = (future_dates - t0).dt.days.values
			plt.plot(future_dates, multi_sigmoid_model(models, future_t), '--')
			legend.append('full model')
			
			for i, x in enumerate(models):
				plt.plot(future_dates, sigmoid_model(x, future_t), '--')
				legend.append(model_names[i])
		
		plt.plot(df['date'], df['cases'], '+')
		legend.append('actual')
		plt.legend(legend)
	
	for name, x in zip(model_names, models):
		print("{:<20} growth: {:.3f}, max: {:>7,.0f}".format(name, x[1], x[0]))
	
	print("Saturation: {:,.0f}".format(sum(x[0] for x in models)))
	plt.show()

def fit_sigmoid(t0, df, population):
	t = (df['date'] - t0).dt.days.values[1:]
	n = df['cases']
	
	dn = n.diff().values[1:]
	
	def _f(x):
		a, b, c = x
		
		phase = np.exp(b) * (t - c)
		log_s = -np.logaddexp(0, -phase)
		log_1ms = -np.logaddexp(0, phase)
		
		# log of lambda, parameter to poisson
		log_l = a + b + log_s + log_1ms
		
		L = np.sum(dn * log_l - np.exp(log_l) - gammaln(dn + 1))
		
		#L += ((a - np.log(population * 0.5)) / 10000000000000)**2
		
		return -L
	
	x0 = np.array([np.log(n.max()), np.log(5/t.max()), 20])
	res = minimize(_f, x0, method = 'Powell')
	if not res.success:
		print(res)
		assert res.success
	x = res.x
	x[:2] = np.exp(x[:2])
	return x

def sigmoid_model(x, t):
	a, b, c = x
	return a * expit(b * (t - c))

def multi_sigmoid_model(xs, t):
	return np.sum([
		sigmoid_model(x, t)
		for x in xs
	], axis = 0)

def get_adjusted_data(date):
	data = {}
	feb_11 = pd.Timestamp(2020, 2, 11)
	china_adj = None
	for name in NAMES:
		df = get_cases(date, name)
		
		if name == 'country/china':
			mask = (df['date'] <= feb_11)
			china_adj = df[mask]['cases'] * 0.25
			df.loc[mask, 'cases'] += china_adj
		elif name == 'coronavirus-cases':
			df.loc[df['date'] <= feb_11, 'cases'] += china_adj
		
		data[name] = df
	return data

if __name__ == '__main__':
	main()
